.PHONY:

# Required System files
CURL_EXE := $(shell which curl)

# STDOUT Formatting
YELLOW := $$(echo "\033[0;33m")
END := $$(echo  "\033[0m")
INFO_HEADER := "**************** "

# Variables
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
ES_DATA_DIR := $(ROOT_DIR)/elasticsearch
ES_LOAD_SCRIPT := $(ES_DATA_DIR)/load-es-data.sh
ES_USERNAME := elastic
ES_PASSWORD := myelasticpassword
ES_BASIC_AUTH := $(shell echo -n "$(ES_USERNAME):$(ES_PASSWORD)" | base64)

#############################################################
# Internal Targets
#############################################################
_ping_elasticsearch_server:
	@echo $(YELLOW)$(INFO_HEADER) "Pinging ElasticSearch on http://localhost:9200" $(END)
	@$(RETRY_CMD) "curl --retry 10 \
    --retry-delay 0 \
    --retry-max-time 40 \
    --retry-connrefuse \
	-H \"Authorization: Basic $(ES_BASIC_AUTH)\" \
	\"http://localhost:9200/_cluster/health?wait_for_status=yellow&timeout=100s&wait_for_no_initializing_shards=true\""
	@echo ""

start:
	@echo $(YELLOW)$(INFO_HEADER) "Starting the following services: elasticsearch, kibana, arranger-server, and arranger-ui" $(END)
	@$(MAKE) _ping_elasticsearch_server
	@echo $(YELLOW)$(INFO_HEADER) Succesfully started all arranger services! $(END)


#############################################################
#  Cleaning targets
#############################################################
# Destroy all non-arranger and non-kibana elasticsearch indices
clean-elastic:
	@echo $(YELLOW)$(INFO_HEADER) "Removing ElasticSearch indices" $(END)
	@$(CURL_EXE) \
		-H "Authorization: Basic $(ES_BASIC_AUTH)" \
		-X GET "http://localhost:9200/_cat/indices" \
		| grep -v kibana \
		| grep -v arranger \
		| grep -v configuration \
		| awk '{ print $$3 }' \
		| xargs -i $(CURL_EXE) -H "Authorization: Basic $(ES_BASIC_AUTH)" -XDELETE "http://localhost:9200/{}?pretty"

	@echo $(YELLOW)$(INFO_HEADER) "ElasticSearch indices removed" $(END)


#############################################################
#  Indexing and ES Targets
#############################################################
# Just delete the documents, not the entire index.
clear-es-documents:
	@echo $(YELLOW)$(INFO_HEADER) "Deleting elasticsearch documents" $(END)
	@$(CURL_EXE) -s -X GET \
		-H "Authorization: Basic $(ES_BASIC_AUTH)" \
		"http://localhost:9200/_cat/indices" \
		| grep -v kibana \
		| grep -v arranger \
		| grep -v configuration \
		| awk '{ print $$3 }'  \
		| sed  's/^/http:\/\/localhost:9200\//' \
		| sed 's/$$/\/_delete_by_query/' \
		| xargs $(CURL_EXE) -XPOST -H "Authorization: Basic $(ES_BASIC_AUTH)" --header 'Content-Type: application/json' -d '{"query":{"match_all":{}}}'

init-es:
	@echo $(YELLOW)$(INFO_HEADER) "Initializing file_centric index" $(END)
	@$(ES_LOAD_SCRIPT) $(ES_DATA_DIR) $(ES_USERNAME) $(ES_PASSWORD)


get-es-indices:
	@echo $(YELLOW)$(INFO_HEADER) "Available indices:" $(END)
	@$(CURL_EXE) -X GET -H "Authorization: Basic $(ES_BASIC_AUTH)"  "localhost:9200/_cat/indices"

get-es-filecentric-content:
	@echo $(YELLOW)$(INFO_HEADER) "files repo content:" $(END)
	@$(CURL_EXE) -X GET -H "Authorization: Basic $(ES_BASIC_AUTH)"  "localhost:9200/files/_search?size=100" | ${JQ_EXE} -e