import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { maintenanceMode } from "common/injectGlobals";
import MaintenancePage from "pages/MaintenancePage";
import { initStore } from "store";

import { getAppElement } from "./services/globalDomNodes.js";
import App from "./App";

import "./index.css";
import "antd/dist/antd.css";

const render = (rootElement) => {
  ReactDOM.render(rootElement, getAppElement());
};

const store = initStore();

if (maintenanceMode) {
  render(
    <Provider store={store}>
      <MaintenancePage />
    </Provider>
  );
} else {
  render(
    <Provider store={store}>
      <App />
    </Provider>
  );
}
