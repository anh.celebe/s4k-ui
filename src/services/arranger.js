import ajax from 'services/ajax';
import { arrangerProjectId, arrangerApiRoot } from 'common/injectGlobals';

export const graphql = (api, queryName = '') => body =>
  api
    ? api({ endpoint: `/${arrangerProjectId}/graphql/${queryName}`, body })
    : ajax.post(urlJoin(arrangerApiRoot, `/${arrangerProjectId}/graphql`), body);