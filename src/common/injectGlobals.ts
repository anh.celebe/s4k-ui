export const getApplicationEnvVar = (envVarName: any) =>
  process.env[`REACT_APP_${envVarName}`];
export const maintenanceMode: Boolean =
  getApplicationEnvVar("MAINTENANCE_MODE") === "true";

export const kfWebRoot: string = getApplicationEnvVar("KF_WEB_ROOT") || "";
export const notionWebRoot: string =
  getApplicationEnvVar("NOTION_WEB_ROOT") || "";
export const kfFacebook: string = getApplicationEnvVar("KF_FACEBOOK") || "";
export const kfTwitter: string = getApplicationEnvVar("KF_TWITTER") || "";
export const kfGithub: string = getApplicationEnvVar("KF_GITHUB") || "";

export const arrangerApiRoot = getApplicationEnvVar("ARRANGER_API");
export const arrangerProjectId = getApplicationEnvVar("PROJECT_ID");

export const activeIndex: string = getApplicationEnvVar("ACTIVE_INDEX") || "";
export const activeIndexName: string =
  getApplicationEnvVar("ACTIVE_INDEX_NAME") || "";
export const activeProjectId: string =
  getApplicationEnvVar("ACTIVE_PROJECT_ID") || "";
