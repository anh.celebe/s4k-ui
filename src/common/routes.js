const ROUTES = {
  admin: "/admin",
  file: "/file",
  login: "/",
  user: "/user",
  profile: "/profile",
  termsConditions: "/termsConditions",
  dashboard: "/dashboard",
  analysis: "/analysis",
  members: "/members",
  /* temporary while constructing the new variant page*/
};

export default ROUTES;
