import React from "react";
// import {
// Arranger,
// CurrentSQON,
// Table,
// Aggregations,
// } from "@kfarranger/components/dist/Arranger";
// import State from "@kfarranger/components/dist/State";
import {
  Arranger,
  CurrentSQON,
  Table,
  Aggregations,
} from "@s4karranger/components/dist/Arranger";
import State from "@s4karranger/components/dist/State";
import { Spin } from "antd";
import { injectGlobal } from "emotion";
import { Layout } from "antd";
import Column from "uikit/Column";
import Row from "uikit/Row";

// import SQONURL from 'components/SQONURL';
import {
  activeIndex,
  activeIndexName,
  activeProjectId,
} from "common/injectGlobals";
import SQONURL from "components/SQONURL";

import { fillCenter } from "theme/tempTheme.module.css";
import "theme/ThemeSwicherArranger/beagle/beagle.css";
import "./styles.scss";

injectGlobal`
  html,
  body,
  #root {
    height: 100vh;
    margin: 0;
  }
`;

const FileRepo = () => {
  return (
    <>
      <SQONURL
        render={(url) => (
          <State
            initial={{
              index: activeIndex,
              graphqlField: activeIndexName,
              projectId: activeProjectId,
            }}
            render={({ index, graphqlField, projectId, update }) => {
              return index && projectId ? (
                <Arranger
                  disableSocket
                  index={index}
                  graphqlField={graphqlField}
                  projectId={projectId}
                  render={(props) => {
                    return (
                      <>
                        <Portal
                          url
                          {...{ ...props, graphqlField, projectId }}
                        />
                      </>
                    );
                  }}
                />
              ) : (
                <div className={fillCenter}>
                  <Spin size={"large"} />
                </div>
              );
            }}
          />
        )}
      />
    </>
  );
};

const Portal = ({ url, style, ...props }) => {
  console.log("props", { ...props });
  return (
    // <div style={{ display: "flex", ...style }}>
    //   <Layout className="arranger-container">
    //     <Aggregations
    //       style={{ width: 300 }}
    //       componentProps={{
    //         getTermAggProps: () => ({
    //           maxTerms: 3,
    //         }),
    //       }}
    //       {...props}
    //     />
    // <Column
    //   className="arranger-table-container"
    //   css={`
    //     position: relative;
    //     flex-grow: 1;
    //     display: flex;
    //     flex-direction: column;
    //   `}
    // >
    //   <Row mb={url.sqon ? 3 : 0}>
    //     <CurrentSQON {...props} />
    //   </Row>
    //   <Column className="arranger-table-wrapper">
    //     <Table {...props} />
    //   </Column>
    // </Column>
    //   </Layout>
    // </div>
    <div style={{ display: "flex", ...style }}>
      <Aggregations
        style={{
          width: 400,
          overflowY: "scroll",
          height: "87vh",
          scrollBehavior: "smooth",
        }}
        componentProps={{
          getTermAggProps: () => ({
            maxTerms: 3,
          }),
        }}
        {...props}
      />

      {/* <Column className="arranger-table-container">
        <Row>
          <CurrentSQON {...props} />
        </Row>
        <Column className="arranger-table-wrapper">
          <Table {...props} />
        </Column>
      </Column> */}

      <div className="customStyleTableContainer">
        <CurrentSQON {...props} />
        <Table {...props} />
      </div>
    </div>
  );
};

export default FileRepo;

// const FileRepo =({...props}) => <SQONURL
//   render={(url)=>(
//     <ArrangerConnectionGuard
//       graphqlField={activeProjectId}
//       render={({ connecting, connectionError }) =>
//           connecting || connectionError ? (
//             <div className={fillCenter}>
//               {connectionError ? (
//                 `Unable to connect to the file repo, please try again later`
//               ) : (
//                 <Spin size={'large'} />
//               )}
//             </div>
//           ) : (
//             <Arranger
//           )
//     />
//   )}
// />
