import React from "react";
import { Layout } from "antd";

import Header from "components/Header";
import Footer from "components/Footer";

import { layoutContainer } from "./Wrapper.module.css";

const { Content, Footer: Ft } = Layout;

const Wrapper = ({ Head = Header, Foot = Footer, Component, ...props }) => (
  <Layout className={layoutContainer}>
    <Head />
    <Content
      style={{
        display: "flex",
        alignItems: "stretch",
        flexDirection: "column",
        overflowY: "auto",
      }}
    >
      <Component {...props} />
    </Content>
    <Ft style={{ padding: "0" }}>
      <Foot />
    </Ft>
  </Layout>
);

export default Wrapper;
