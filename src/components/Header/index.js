import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Layout } from "antd";
import {
  FolderOutlined,
  HomeOutlined,
  BarChartOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";

// import ROUTES from "common/routes";
import ROUTES from "common/routes";
import StackLayout from "layout_ferlab/StackLayout";
import logoPath from "assets/logo.png";
import logoPathIcon from "assets/icon-baby.png";

import { NavBarList, NavLink } from "./ui";

const { Header } = Layout;

const NavigationToolBar = (props) => {
  const { history } = props;
  const currentPathName = history.location.pathname;
  return (
    <Header className="headerContainer">
      {/* <div className="gradientAccent" /> */}
      <StackLayout className="headerContent" vertical={true}>
        <StackLayout>
          <Link to={ROUTES.dashboard}>
            <img
              src={logoPathIcon}
              alt="Kids First Logo ICON"
              className={"logoIcon"}
            />
            <img src={logoPath} alt="Kids First Logo" className={"logo"} />
          </Link>
          <NavBarList ml={40}>
            <li>
              <NavLink currentPathName={currentPathName} to={ROUTES.dashboard}>
                <HomeOutlined /> Dashboard
              </NavLink>
            </li>
            <li>
              <NavLink currentPathName={currentPathName} to={ROUTES.file}>
                <FolderOutlined /> File Repository
              </NavLink>
            </li>
            <li>
              <NavLink currentPathName={currentPathName} to={ROUTES.analysis}>
                <BarChartOutlined /> Analysis
              </NavLink>
            </li>
            <li>
              <NavLink currentPathName={currentPathName} to={ROUTES.members}>
                <UsergroupAddOutlined /> Members
              </NavLink>
            </li>
          </NavBarList>
        </StackLayout>
      </StackLayout>
    </Header>
  );
};

export default withRouter(NavigationToolBar);
