import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction";
import thunk from "redux-thunk";

import rootReducers from "./reducers";
import subscibe from "./subscribers";

export let store = null;
export const initStore = (preloadedState = {}) => {
  const composeEnhancers = composeWithDevTools({
    // Specify extension’s options like name,
    //  actionsBlacklist, actionsCreators, serialize...
  });
  const enhancer = composeEnhancers(applyMiddleware(thunk));

  store = createStore(rootReducers, preloadedState, enhancer);
  subscibe(store);
  return store;
};
