import React, { lazy, Suspense } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import ROUTES from "common/routes";
import { maintenanceMode } from "common/injectGlobals";
import Wrapper from "components/Wrapper/index";
import { Spinner } from "uikit/Spinner";
import DashboardPage from "pages/DashboardPage/index";
import AnalysisPage from "pages/AnalysisPage/index";
import MembersPage from "pages/MembersPage/index";

import "./App.css";

console.log("maintenaceMode", maintenanceMode);

const protectRoute = ({ WrapperPage = Wrapper, ...props }) => {
  return <WrapperPage {...props} />;
};

const FileRepo = lazy(() => import("components/FileRepo/index"));
// const AnalysisPage = lazy(() => import("pages/AnalysisPage/index"));
// const MembersPage = lazy(() => import("pages/MembersPage/index"));

function App() {
  return (
    <Router>
      <Suspense fallback={<Spinner className={"spinner"} size={"large"} />}>
        <Switch>
          <Route
            path={ROUTES.dashboard}
            exact
            render={(props) =>
              protectRoute({
                Component: DashboardPage,
                WrapperPage: Wrapper,
                ...props,
              })
            }
          />
          <Route
            path={ROUTES.file}
            exact
            render={(props) =>
              protectRoute({
                Component: FileRepo,
                WrapperPage: Wrapper,
                ...props,
              })
            }
          />
          <Route
            path={ROUTES.members}
            exact
            render={(props) =>
              protectRoute({
                Component: MembersPage,
                WrapperPage: Wrapper,
                ...props,
              })
            }
          />
          <Route
            path={ROUTES.analysis}
            exact
            render={(props) =>
              protectRoute({
                Component: AnalysisPage,
                WrapperPage: Wrapper,
                ...props,
              })
            }
          />
          <Route exact path="/">
            <Redirect to="/dashboard" />
          </Route>
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;
