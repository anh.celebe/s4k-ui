import React from "react";
import ScrollView from "@ferlab/ui/core/layout/ScrollView";
import { Layout } from "antd";

import PageContent from "components/Layout/PageContent";

import styles from "./styles.module.scss";

const DashboardPage = () => {
  return (
    <Layout className={styles.layout}>
      <ScrollView className={styles.scrollContent}>
        <PageContent title="Analysis Page"></PageContent>
      </ScrollView>
    </Layout>
  );
};

export default DashboardPage;
