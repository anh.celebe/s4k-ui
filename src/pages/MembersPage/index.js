import React from "react";
import ScrollView from "@ferlab/ui/core/layout/ScrollView";
import { Layout } from "antd";

import PageContent from "components/Layout/PageContent";

import styles from "./styles.module.scss";

const MembersPage = () => {
  return (
    <Layout className={styles.layout}>
      <ScrollView className={styles.scrollContent}>
        <PageContent title="Members Page"></PageContent>
      </ScrollView>
    </Layout>
  );
};

export default MembersPage;
