import React from "react";
import ScrollView from "@ferlab/ui/core/layout/ScrollView";
import { Layout } from "antd";

import PageContent from "components/Layout/PageContent";
import FileRepo from "components/FileRepo";

import styles from "./styles.module.scss";

const FileRepositoryPage = () => {
  return (
    <Layout className={styles.layout}>
      <ScrollView className={styles.scrollContent}>
        <PageContent title="File Repository Page">
          <FileRepo />
        </PageContent>
      </ScrollView>
    </Layout>
  );
};

export default FileRepositoryPage;
