#!/bin/bash
set -euo pipefail

es_data_dir=$1
es_username=$2
es_password=$3

if [ ! -d $es_data_dir ]; then
	echo "The directory $es_data_dir does not exist"
	exit 1
fi

es_doc_dir=$es_data_dir/files
es_index_config_file=$es_data_dir/config.json


es_basic_auth=$(echo -n "$es_username:$es_password" | base64)

echo "Creating files index"
curl -sL -XPUT \
	-H "Content-Type: application/json" \
	-H "Authorization: Basic $es_basic_auth" \
	http://localhost:9200/files \
	-d "@${es_index_config_file}"
echo""

for f in $es_doc_dir/*.json
do
	#echo "Loading document: $f"
	object_id=$(echo $f | sed 's/\.json//' | sed 's/.*\///g')

	curl -sL -XPUT \
		-H "Content-Type: application/json" \
		-H "Authorization: Basic $es_basic_auth" \
		http://localhost:9200/files/_doc/$object_id \
		-d "@${f}"
	echo ""
done


